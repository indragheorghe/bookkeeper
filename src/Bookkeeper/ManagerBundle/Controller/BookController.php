<?php

namespace Bookkeeper\ManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Bookkeeper\ManagerBundle\Form\BookType;
use Bookkeeper\ManagerBundle\Entity\Book;

class BookController extends Controller{

	public function indexAction(){
		// display all books in the book manager app

		$em = $this->getDoctrine()->getManager();

		$book= $em->getRepository('BookkeeperManagerBundle:Book')->findAll();

		return $this->render('BookkeeperManagerBundle:Book:index.html.twig', 
			array('books'=>$book
		));
	}

	public function showAction($id){
		// look at a single book
		$em= $this->getDoctrine()->getManager();
		$book= $em->getRepository('BookkeeperManagerBundle:Book')->find($id);

		$delete_form = $this->createFormBuilder()
			->setAction($this->generateURL('book_delete', array('id'=>$id)))
			->setMethod('DELETE')
			->add('submit', 'submit', array('label'=>'Delete book'))
			->getForm();

		return $this->render('BookkeeperManagerBundle:Book:show.html.twig', array(
			'book'=>$book,
			'delete_form'=>$delete_form->createView()
		));
	}

	public function newAction(){
		// add new book
		$book = new Book();
		$form= $this->createForm(new BookType(), $book, array(
			'action'=>$this->generateURL('book_create'),
			'method'=>'POST')
		);

		$form->add('submit', 'submit', array(
			'label'=>'Create book')
		);

		return $this->render('BookkeeperManagerBundle:Book:new.html.twig', array(
			'form'=>$form->createView()
			));
	}

	public function createAction(Request $request){
		// process adding new book

		$book = new Book();
		$form= $this->createForm(new BookType(), $book, array(
			'action'=>$this->generateURL('book_create'),
			'method'=>'POST')
		);

		$form->add('submit', 'submit', array(
			'label'=>'Create book')
		);

		$form-> handleRequest($request);

		if($form->isValid()){
			$em = $this->getDoctrine()->getManager();
			// trimitem datele catre doctrine
			$em->persist($book);
			// adaugam datele
			$em->flush();

			// cream o sesiune in care sa afisam mesajul. La termnarea sesiunii ca disparea si mesajul
			$this->get('session')->getFlashBag()->add('msg', 'Book created');

			return $this->redirect($this->generateURL('book_show', array('id'=>$book->getId())));
		}

		// in orice alt caz aratam forma cu mesaje de eroare
		$this->get('session')->getFlashBag()->add('msg', 'Something went wrong');
		return $this->render('BookkeeperManagerBundle:Book:new.html.twig', array('form'=>$form->createView()));

	}

	public function editAction($id){
		// edit book
		$em= $this->getDoctrine()->getManager();
		$book = $em->getRepository('BookkeeperManagerBundle:Book')->find($id);

		$form= $this->createForm(new BookType(), $book, array(
			'action'=>$this->generateURL('book_update', array('id'=>$book->getId())),
			'method'=>'PUT'));
		$form->add('submit', 'submit', array('label'=>'Update book'));

		return $this->render('BookkeeperManagerBundle:Book:edit.html.twig', array('form'=>$form->createView()));
	}

	public function updateAction(Request $request, $id){
		// calle when we edit a book, this processes the form submission from editAction
		$em= $this->getDoctrine()->getManager();
		$book = $em->getRepository('BookkeeperManagerBundle:Book')->find($id);

		$form= $this->createForm(new BookType(), $book, array(
			'action'=>$this->generateURL('book_update', array('id'=>$book->getId())),
			'method'=>'PUT'));
		$form->add('submit', 'submit', array('label'=>'Update book'));
		$form->handleRequest($request);

		if($form->isValid()){
			$em->flush();
			$this->get('session')->getFlashBag()->add('msg', 'Book updated');

			$this->redirect($this->generateURL('book_show', array('id'=>$id)));
		}

		return $this->render('BookkeeperManagerBundle:Book:edit.html.twig', array('form'=>$form->createView()));
	}

	public function deleteAction(Request $request, $id){
		// called when we delete a book
		$delete_form = $this->createFormBuilder()
			->setAction($this->generateURL('book_delete', array('id'=>$id)))
			->setMethod('DELETE')
			->add('submit', 'submit', array('label'=>'Delete book'))
			->getForm();
		$delete_form->handleRequest($request);
		
		if($delete_form->isValid()){
			$em = $this->getDoctrine()->getManager(); //create manager
			$book =$em->getRepository('BookkeeperManagerBundle:Book')->find($id);
			$em->remove($book);
			$em->flush();
		}

		$this->get('session')->getFlashBag()->add('msg', 'Book deleted');

		return $this->redirect($this->generateURL('book'));

	}

}